<?php

namespace Avanti\MosaicManager\Block;

use Exception;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\StoreManagerInterface;
use Avanti\MosaicManager\Helper\Data as mosaicHelper;
use Avanti\MosaicManager\Model\ComponentFactory;
use Avanti\MosaicManager\Model\MosaicFactory;
use Avanti\MosaicManager\Model\ResourceModel\Component\Collection as ComponentCollection;
use Avanti\MosaicManager\Model\ResourceModel\Mosaic\Collection as MosaicCollection;

/**
 * Class Mosaic
 * @package Avanti\MosaicManager\Block
 */
class Index extends Template
{
    /**
     * @type mosaicHelper
     */
    public $helperData;

    /**
     * @type StoreManagerInterface
     */
    protected $store;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var DateTime
     */
    protected $_date;

    /**
     * @var FilterProvider
     */
    public $filterProvider;

    /**
     * @var FilterProvider
     */
    protected $componentCollection;

    /**
     * @var FilterProvider
     */
    protected $mosaicCollection;

    /**
     * @var ComponentFactory
     */
    protected $componentFactory;

    /**
     * @var ComponentFactory
     */
    protected $mosaicFactory;

    /**
     * Mosaic constructor.
     *
     * @param Template\Context $context
     * @param mosaicHelper $helperData
     * @param CustomerRepositoryInterface $customerRepository
     * @param DateTime $dateTime
     * @param FilterProvider $filterProvider
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        mosaicHelper $helperData,
        CustomerRepositoryInterface $customerRepository,
        DateTime $dateTime,
        FilterProvider $filterProvider,
        ComponentCollection $componentCollection,
        ComponentFactory $componentFactory,
        MosaicFactory $mosaicFactory,
        MosaicCollection $mosaicCollection,
        array $data = []
    ) {
        $this->helperData = $helperData;
        $this->customerRepository = $customerRepository;
        $this->store = $context->getStoreManager();
        $this->_date = $dateTime;
        $this->filterProvider = $filterProvider;
        $this->componentCollection = $componentCollection;
        $this->mosaicCollection = $mosaicCollection;
        $this->componentFactory = $componentFactory;
        $this->mosaicFactory = $mosaicFactory;
        parent::__construct($context, $data);
    }

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Avanti_MosaicManager::mosaicmanager.phtml');
    }

    /**
     * Get Mosaic Id
     * @return string
     */
    public function getMosaicId()
    {
        if (isset($this->_data["mosaic_id"])) {
            return $this->_data["mosaic_id"];
        }

        return null;
    }

    /**
     * Get Mosaic Code
     * @return string|null
     */
    private function getMosaicCode()
    {
        if (isset($this->_data["code"])) {
            return $this->_data["code"];
        }

        return null;
    }

    /**
     * @param $content
     *
     * @return string
     * @throws Exception
     */
    public function getPageFilter($content)
    {
        return $this->filterProvider->getPageFilter()->filter($content);
    }

    /**
     * @return array
     */
    public function getAllComponents()
    {
        return $this->componentCollection->getData();
    }

    /**
     * @return array
     */
    public function getAllMosaics()
    {
        $this->mosaicCollection->addFilter('active', 1);
        return $this->mosaicCollection->getData();
    }

    /**
     * @return array
     */
    public function getComponentsByMosaicId()
    {
        $collection = [];
        if ($this->getMosaicId()) {
            $collection = $this->helperData->getComponentCollection($this->getMosaicId());
        }

        return $collection;
    }

    /**
     * @return array
     */
    public function getComponentsByMosaicCode()
    {
        $collection = [];
        if (!$this->getMosaicCode()) {
            return $collection;
        }

        if (empty($this->getMosaicIdByCode())) {
            return $collection;
        }

        return $this->helperData->getComponentCollection($this->getMosaicIdByCode());
    }

    /**
     * @return array
     */
    private function getMosaicIdByCode()
    {
        $collection = $this->mosaicFactory->create()
            ->getCollection()
            ->addFilter('code', $this->getMosaicCode())
            ->addFilter('active', 1);

        if ($collection->count() > 0) {
            $collectionData = $collection->getData();
            return $collectionData[0]['mosaic_id'];
        }
        return [];
    }
}
