<?php

namespace Avanti\MosaicManager\Block\Adminhtml\Mosaic;

use Magento\Framework\Registry;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Json\EncoderInterface;
use Avanti\MosaicManager\Block\Adminhtml\Mosaic\Tab\Component;

class AssignComponents extends Template
{
    /**
     * Block template.
     *
     * @var string
     */
    protected $_template = 'Avanti_MosaicManager::mosaic/edit/assign_components.phtml';

    /**
     * @var Component
     */
    protected $blockGrid;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * AssignComponents constructor.
     *
     * @param Context          $context
     * @param Registry         $registry
     * @param EncoderInterface $jsonEncoder
     * @param array            $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        EncoderInterface $jsonEncoder,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->jsonEncoder = $jsonEncoder;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve instance of grid block.
     *
     * @return \Magento\Framework\View\Element\BlockInterface
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBlockGrid()
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(Component::class, 'avanti_mosaicmanager.mosaic.component.grid');
        }

        return $this->blockGrid;
    }

    /**
     * Return HTML of grid block.
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getBlockGrid()->toHtml();
    }

    /**
     * @return string
     */
    public function getComponentsJson()
    {
        $components = $this->getMosaic()->getComponentsPosition();
        if (!empty($components)) {
            return $this->jsonEncoder->encode($components);
        }

        return '{}';
    }

    /**
     * Retrieve current mosaic instance.
     *
     * @return array|null
     */
    public function getMosaic()
    {
        return $this->registry->registry('avanti_mosaicmanager_mosaic');
    }
}
