<?php

namespace Avanti\MosaicManager\Block\Adminhtml\Mosaic\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class NewComponentButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        if ($this->getModelId()) {
            $data = [
                'label' => __('New Component'),
                'class' => 'save',
                'on_click' => sprintf("location.href = '%s';", $this->getNewComponentUrl()),
                'sort_order' => 20,
            ];
        }

        return $data;
    }

    /**
     * Get URL for delete button.
     *
     * @return string
     */
    public function getNewComponentUrl()
    {
        return $this->getUrl('*/component/new', ['mosaic_id' => $this->getModelId()]);
    }
}
