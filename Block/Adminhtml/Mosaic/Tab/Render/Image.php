<?php

namespace Avanti\MosaicManager\Block\Adminhtml\Mosaic\Tab\Render;

use Magento\Backend\Block\Context;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;
use Avanti\MosaicManager\Helper\Data as MosaicManagerHelper;

/**
 * Class GridImage.
 */
class Image extends AbstractRenderer
{
    /**
     * @var MosaicManagerHelper
     */
    private $helper;

    /**
     * @param Context            $context
     * @param MosaicManagerHelper $helper
     * @param array              $data
     */
    public function __construct(
        Context $context,
        MosaicManagerHelper $helper,
        array $data = []
    ) {
        $this->helper = $helper;

        parent::__construct($context, $data);
    }

    /**
     * Render Component Image.
     *
     * @param DataObject $row
     *
     * @return string
     */
    public function render(DataObject $row)
    {
        if ($row->getData($this->getColumn()->getIndex())) {
            $imageUrl = $this->helper->getImageUrl($row->getData($this->getColumn()->getIndex()));

            return '<img src="'.$imageUrl.'" width=\'150\' class="admin__control-thumbnail"/>';
        }

        return '';
    }
}
