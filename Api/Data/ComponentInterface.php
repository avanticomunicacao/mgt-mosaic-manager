<?php

namespace Avanti\MosaicManager\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface ComponentInterface extends ExtensibleDataInterface
{
    const COMPONENT_ID = 'component_id';
    const NAME = 'name';
    const IMAGE = 'image';
    const URL = 'url';
    const ALT = 'alt';
    const TEXT_ONE = 'text_one';
    const TEXT_TWO = 'text_two';
    const TEXT_THREE = 'text_three';
    const TEXT_FOUR = 'text_four';
    const TEXT_FIVE = 'text_five';
    const TEXT_SIX = 'text_six';
    const TEXT_SEVEN = 'text_seven';
    const TEXT_EIGHT = 'text_eight';
    const TEXT_NINE = 'text_nine';
    const TEXT_TEN = 'text_ten';
    const UPDATED_AT = 'updated_at';
    const CREATED_AT = 'created_at';

    /**
     * Get component_id.
     *
     * @return string|null
     */
    public function getComponentId();

    /**
     * Set component_id.
     *
     * @param string $componentId
     *
     * @return ComponentInterface
     */
    public function setComponentId($componentId);

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ComponentInterface
     */
    public function setName($name);

    /**
     * Get image.
     *
     * @return string|null
     */
    public function getImage();

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return ComponentInterface
     */
    public function setImage($image);

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getUrl();

    /**
     * Set url.
     *
     * @param string $url
     *
     * @return ComponentInterface
     */
    public function setUrl($url);

    /**
     * Get alt.
     *
     * @return string|null
     */
    public function getAlt();

    /**
     * Set alt.
     *
     * @param string $alt
     *
     * @return ComponentInterface
     */
    public function setAlt($alt);

    /**
     * Get text one.
     *
     * @return string|null
     */
    public function getTextOne();

    /**
     * Set text one.
     *
     * @param string $textOne
     *
     * @return ComponentInterface
     */
    public function setTextOne($textOne);

    /**
     * Get text two.
     *
     * @return string|null
     */
    public function getTextTwo();

    /**
     * Set text two.
     *
     * @param string $textTwo
     *
     * @return ComponentInterface
     */
    public function setTextTwo($textTwo);

    /**
     * Get text three.
     *
     * @return string|null
     */
    public function getTextThree();

    /**
     * Set text three.
     *
     * @param string $textThree
     *
     * @return ComponentInterface
     */
    public function setTextThree($textThree);

    /**
     * Get text four.
     *
     * @return string|null
     */
    public function getTextFour();

    /**
     * Set text four.
     *
     * @param string $textFour
     *
     * @return ComponentInterface
     */
    public function setTextFour($textFour);

    /**
     * Get text five.
     *
     * @return string|null
     */
    public function getTextFive();

    /**
     * Set text five.
     *
     * @param string $textFive
     *
     * @return ComponentInterface
     */
    public function setTextFive($textFive);

    /**
     * Get text six.
     *
     * @return string|null
     */
    public function getTextSix();

    /**
     * Set text six.
     *
     * @param string $textSix
     *
     * @return ComponentInterface
     */
    public function setTextSix($textSix);

    /**
     * Get text seven.
     *
     * @return string|null
     */
    public function getTextSeven();

    /**
     * Set text seven.
     *
     * @param string $textSeven
     *
     * @return ComponentInterface
     */
    public function setTextSeven($textSeven);

    /**
     * Get text eight.
     *
     * @return string|null
     */
    public function getTextEight();

    /**
     * Set text eight.
     *
     * @param string $textEight
     *
     * @return ComponentInterface
     */
    public function setTextEight($textEight);

    /**
     * Get text nine.
     *
     * @return string|null
     */
    public function getTextNine();

    /**
     * Set text nine.
     *
     * @param string $textNine
     *
     * @return ComponentInterface
     */
    public function setTextNine($textNine);

    /**
     * Get text ten.
     *
     * @return string|null
     */
    public function getTextTen();

    /**
     * Set text ten.
     *
     * @param string $textTen
     *
     * @return ComponentInterface
     */
    public function setTextTen($textTen);

    /**
     * Get created_at.
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at.
     *
     * @param string $createdAt
     *
     * @return ComponentInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at.
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at.
     *
     * @param string $updatedAt
     *
     * @return ComponentInterface
     */
    public function setUpdatedAt($updatedAt);
}
