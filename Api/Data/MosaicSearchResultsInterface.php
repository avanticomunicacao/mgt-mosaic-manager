<?php

namespace Avanti\MosaicManager\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface MosaicSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Mosaic list.
     *
     * @return \Avanti\MosaicManager\Api\Data\MosaicInterface[]
     */
    public function getItems();

    /**
     * Set name list.
     *
     * @param \Avanti\MosaicManager\Api\Data\MosaicInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}
