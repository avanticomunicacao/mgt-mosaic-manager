<?php

namespace Avanti\MosaicManager\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ComponentSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Component list.
     *
     * @return \Avanti\MosaicManager\Api\Data\ComponentInterface[]
     */
    public function getItems();

    /**
     * Set name list.
     *
     * @param \Avanti\MosaicManager\Api\Data\ComponentInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}
