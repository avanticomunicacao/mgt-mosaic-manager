<?php

namespace Avanti\MosaicManager\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface MosaicInterface extends ExtensibleDataInterface
{
    const MOSAIC_ID = 'mosaic_id';
    const NAME = 'name';
    const CODE = 'code';
    const ACTIVE = 'active';
    const UPDATED_AT = 'updated_at';
    const CREATED_AT = 'created_at';

    /**
     * Get mosaic_id.
     *
     * @return string|null
     */
    public function getMosaicId();

    /**
     * Set mosaic_id.
     *
     * @param string $mosaicId
     *
     * @return \Avanti\MosaicManager\Api\Data\MosaicInterface
     */
    public function setMosaicId($mosaicId);

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return \Avanti\MosaicManager\Api\Data\MosaicInterface
     */
    public function setName($name);

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode();

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return \Avanti\MosaicManager\Api\Data\MosaicInterface
     */
    public function setCode($code);

    /**
     * Get active.
     *
     * @return string|null
     */
    public function getActive();

    /**
     * Set active.
     *
     * @param string $active
     *
     * @return \Avanti\MosaicManager\Api\Data\MosaicInterface
     */
    public function setActive($active);

    /**
     * Get created_at.
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at.
     *
     * @param string $createdAt
     *
     * @return \Avanti\MosaicManagerPro\Api\Data\MosaicInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at.
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at.
     *
     * @param string $updatedAt
     *
     * @return \Avanti\MosaicManagerPro\Api\Data\MosaicInterface
     */
    public function setUpdatedAt($updatedAt);
}
