<?php

namespace Avanti\MosaicManager\Api;

use Avanti\MosaicManager\Api\Data\ComponentInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteriaInterface;
use Avanti\MosaicManager\Api\Data\ComponentSearchResultsInterface;

interface ComponentRepositoryInterface
{
    /**
     * Save Component.
     *
     * @param ComponentInterface $component
     *
     * @return ComponentInterface
     *
     * @throws LocalizedException
     */
    public function save(ComponentInterface $component);

    /**
     * Retrieve Component.
     *
     * @param string $componentId
     *
     * @return ComponentInterface
     *
     * @throws LocalizedException
     */
    public function get($componentId);

    /**
     * Retrieve Component matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return ComponentSearchResultsInterface
     *
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Component.
     *
     * @param ComponentInterface $component
     *
     * @return bool true on success
     *
     * @throws LocalizedException
     */
    public function delete(ComponentInterface $component);

    /**
     * Delete Component by ID.
     *
     * @param string $componentId
     *
     * @return bool true on success
     *
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($componentId);
}
