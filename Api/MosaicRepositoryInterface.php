<?php

namespace Avanti\MosaicManager\Api;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteriaInterface;
use Avanti\MosaicManager\Api\Data\MosaicInterface;
use Avanti\MosaicManager\Api\Data\MosaicSearchResultsInterface;

interface MosaicRepositoryInterface
{
    /**
     * Save Mosaic.
     *
     * @param MosaicInterface $mosaic
     *
     * @return MosaicInterface
     *
     * @throws LocalizedException
     */
    public function save(MosaicInterface $mosaic);

    /**
     * Retrieve Mosaic.
     *
     * @param string $mosaicId
     *
     * @return MosaicInterface
     *
     * @throws LocalizedException
     */
    public function get($mosaicId);

    /**
     * Retrieve Mosaic matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return MosaicSearchResultsInterface
     *
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Mosaic.
     *
     * @param MosaicInterface $mosaic
     *
     * @return bool true on success
     *
     * @throws LocalizedException
     */
    public function delete(MosaicInterface $mosaic);

    /**
     * Delete Mosaic by ID.
     *
     * @param string $mosaicId
     *
     * @return bool true on success
     *
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($mosaicId);
}
