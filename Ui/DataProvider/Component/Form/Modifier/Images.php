<?php

namespace Avanti\MosaicManager\Ui\DataProvider\Component\Form\Modifier;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Registry;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Avanti\MosaicManager\Model\Component;
use Avanti\MosaicManager\Helper\Data as MosaicManagerHelper;

class Images implements ModifierInterface
{
    const FIELD_LABEL_IMAGE = 'image';

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var Component
     */
    private $component;

    /**
     * @var MosaicManagerHelper
     */
    private $helper;

    /**
     * @param Registry $registry
     * @param MosaicManagerHelper $helper
     */
    public function __construct(Registry $registry, MosaicManagerHelper $helper)
    {
        $this->registry = $registry;
        $this->helper = $helper;
    }

    /**
     * @return Component
     *
     * @throws NotFoundException
     */
    public function getComponent()
    {
        if (null !== $this->component) {
            return $this->component;
        }

        if ($component = $this->registry->registry('avanti_mosaicmanager_component')) {
            return $this->component = $component;
        }

        throw new NotFoundException(__("The component wasn't registered."));
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        $componentId = $this->getComponent()->getComponentId();
        if (isset($data[$componentId][self::FIELD_LABEL_IMAGE])) {
            $imageName = $data[$componentId][self::FIELD_LABEL_IMAGE];
            $data[$componentId][self::FIELD_LABEL_IMAGE] = [[
                'name' => $imageName,
                'url' => $this->helper->getImageUrl($imageName),
            ]];
        }

        return $data;
    }
}
