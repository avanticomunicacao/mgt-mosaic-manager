<?php

namespace Avanti\MosaicManager\Controller\Adminhtml\Mosaic;

use Avanti\MosaicManager\Controller\Adminhtml\Mosaic;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Avanti\MosaicManager\Model\MosaicFactory;

class Edit extends Mosaic
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var MosaicFactory
     */
    private $mosaicFactory;

    /**
     * @param Context     $context
     * @param Registry    $registry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        PageFactory $resultPageFactory,
        MosaicFactory $mosaicFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->mosaicFactory = $mosaicFactory;
        parent::__construct($context, $registry);
    }

    /**
     * Edit action.
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('mosaic_id');
        $mosaic = $this->mosaicFactory->create();

        if ($id) {
            $mosaic->load($id);
            if (!$mosaic->getMosaicId()) {
                $this->messageManager->addErrorMessage(__('This Mosaic no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->registry->register('avanti_mosaicmanager_mosaic', $mosaic);

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Mosaic') : __('New Mosaic'),
            $id ? __('Edit Mosaic') : __('New Mosaic')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Mosaics'));
        $resultPage->getConfig()->getTitle()->prepend(
            $mosaic->getMosaicId() ? __('Edit Mosaic %1', $mosaic->getMosaicId()) : __('New Mosaic')
        );

        return $resultPage;
    }
}
