<?php

namespace Avanti\MosaicManager\Controller\Adminhtml\Mosaic;

use Avanti\MosaicManager\Controller\Adminhtml\Mosaic;
use Avanti\MosaicManager\Model\MosaicFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Delete extends Mosaic
{
    /**
     * @var MosaicFactory
     */
    private $mosaicFactory;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param MosaicFactory $mosaicFactory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        MosaicFactory $mosaicFactory
    ) {
        $this->mosaicFactory = $mosaicFactory;
        parent::__construct($context, $registry);
    }
    /**
     * Delete action.
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $mosaicId = $this->getRequest()->getParam('mosaic_id');
        if ($mosaicId) {
            try {
                $mosaicModel = $this->mosaicFactory->create();
                $mosaicModel->load($mosaicId);
                $mosaicModel->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the Mosaic.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['mosaic_id' => $mosaicId]);
            }
        }
        $this->messageManager->addErrorMessage(__("We can't find a Mosaic to delete."));
        return $resultRedirect->setPath('*/*/');
    }
}
