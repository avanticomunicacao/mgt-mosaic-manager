<?php

namespace Avanti\MosaicManager\Controller\Adminhtml\Mosaic;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Request\DataPersistorInterface;
use Avanti\MosaicManager\Model\MosaicFactory;

class Save extends Action
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    private $mosaicFactory;

    /**
     * [__construct description].
     *
     * @param Context                $context
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        MosaicFactory $mosaicFactory
    ){
        $this->dataPersistor = $dataPersistor;
        $this->mosaicFactory = $mosaicFactory;
        parent::__construct($context);
    }

    /**
     * Save action.
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $mosaicId = $this->getRequest()->getParam('mosaic_id');
            if ($mosaicId === null) {
                $data['is_new_mosaic'] = true;
            }
            $mosaic = $this->mosaicFactory->create()->load($mosaicId);
            if (!$mosaic->getMosaicId() && $mosaicId) {
                $this->messageManager->addErrorMessage(__('This Mosaic no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $mosaic->setData($data);
            if (isset($data['mosaic_components'])
                && is_string($data['mosaic_components'])
                && !$mosaic->getComponentsReadonly()
            ) {
                $components = json_decode($data['mosaic_components'], true);
                $mosaic->setPostedComponents($components);
            }

            try {
                $mosaic->save();
                $this->messageManager->addSuccessMessage(__('You saved the Mosaic.'));
                $this->dataPersistor->clear('avanti_mosaicmanager_mosaic');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['mosaic_id' => $mosaic->getId()]);
                }

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Mosaic.'));
            }

            $this->dataPersistor->set('avanti_mosaicmanager_mosaic', $data);

            return $resultRedirect->setPath('*/*/edit', ['mosaic_id' => $this->getRequest()->getParam('mosaic_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
