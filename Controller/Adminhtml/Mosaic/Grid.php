<?php

namespace Avanti\MosaicManager\Controller\Adminhtml\Mosaic;

use Avanti\MosaicManager\Controller\Adminhtml\Mosaic;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Registry;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\View\LayoutFactory;
use Avanti\MosaicManager\Model\MosaicFactory;
use Avanti\MosaicManager\Block\Adminhtml\Mosaic\Tab\Component as MosaicComponentsGrid;

class Grid extends Mosaic
{
    /**
     * @var RawFactory
     */
    private $resultRawFactory;

    /**
     * @var LayoutFactory
     */
    private $layoutFactory;

    /**
     * @var MosaicFactory
     */
    private $mosaicFactory;

    /**
     * @param Context       $context
     * @param Registry      $registry
     * @param RawFactory    $resultRawFactory
     * @param LayoutFactory $layoutFactory
     * @param MosaicFactory $mosaicFactory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        RawFactory $resultRawFactory,
        LayoutFactory $layoutFactory,
        MosaicFactory $mosaicFactory
    ) {
        $this->resultRawFactory = $resultRawFactory;
        $this->layoutFactory = $layoutFactory;
        $this->mosaicFactory = $mosaicFactory;

        parent::__construct($context, $registry);
    }

    /**
     * Initialize requested mosaic and put it into registry.
     *
     * @return \Avanti\MosaicManager\Model\Mosaic|false
     */
    protected function initMosaic()
    {
        $mosaicId = (int) $this->getRequest()->getParam('mosaic_id', false);
        $mosaic = $this->mosaicFactory->create();

        if ($mosaicId) {
            $mosaic->load($mosaicId);
        }

        $this->registry->register('avanti_mosaicmanager_mosaic', $mosaic);

        return $mosaic;
    }

    /**
     * Grid Action
     * Display list of products related to current category.
     *
     * @return Raw
     */
    public function execute()
    {
        $mosaic = $this->initMosaic();
        if (!$mosaic) {
            /** @var Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('avanti_mosaicmanager/*/', ['_current' => true, 'id' => null]);
        }

        /** @var Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw->setContents(
            $this->layoutFactory->create()->createBlock(
                MosaicComponentsGrid::class,
                'avanti_mosaicmanager.mosaic.component.grid'
            )->toHtml()
        );
    }
}
