<?php

namespace Avanti\MosaicManager\Controller\Adminhtml\Component;

use Avanti\MosaicManager\Controller\Adminhtml\Component;
use Avanti\MosaicManager\Model\ComponentFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Delete extends Component
{
    private $componentFactory;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @param Context     $context
     * @param Registry    $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        ComponentFactory $componentFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->componentFactory = $componentFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Delete action.
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $componentId = $this->getRequest()->getParam('component_id');
        if ($componentId) {
            try {
                $componentModel = $this->componentFactory->create();
                $componentModel->load($componentId);
                $componentModel->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the Component.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['component_id' => $componentId]);
            }
        }
        $this->messageManager->addErrorMessage(__("We can't find a Component to delete."));
        return $resultRedirect->setPath('*/*/');
    }
}
