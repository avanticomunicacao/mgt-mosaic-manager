<?php

namespace Avanti\MosaicManager\Controller\Adminhtml\Component;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Avanti\MosaicManager\Model\ComponentFactory;
use Avanti\MosaicManager\Model\MosaicFactory;

class Save extends Action
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var ComponentFactory
     */
    private $componentFactory;

    /**
     * @var MosaicFactory
     */
    private $mosaicFactory;

    /**
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param ComponentFactory $componentFactory
     * @param MosaicFactory $mosaicFactory
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        ComponentFactory $componentFactory,
        MosaicFactory $mosaicFactory
    ) {
        $this->componentFactory = $componentFactory;
        $this->mosaicFactory = $mosaicFactory;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action.
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $mosaicId = $this->dataPersistor->get('avanti_mosaicmanager_mosaic_id');

        if ($data) {
            $componentId = $this->getRequest()->getParam('component_id');
            $component = $this->componentFactory->create()->load($componentId);
            $mosaic = $this->mosaicFactory->create()->load($mosaicId);
            $mosaicData = $mosaic->getData();

            if (!$component->getId() && $componentId) {
                $this->messageManager->addErrorMessage(__('This Component no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            $component->setData($data);
            try {
                $component->save();
                $this->messageManager->addSuccessMessage(__('You saved the Component.'));
                $this->dataPersistor->clear('avanti_mosaicmanager_component');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['component_id' => $component->getComponentId()]);
                }

                if ($mosaicId) {
                    $mosaicData['component_id'] = $component->getId();
                    $mosaic->setData($mosaicData);
                    $mosaic->save();
                    $this->dataPersistor->clear('avanti_mosaicmanager_mosaic_id');
                    return $resultRedirect->setPath('*/mosaic/edit', ['mosaic_id' => $mosaicId]);
                }

                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Component.'));
            }

            $this->dataPersistor->set('avanti_mosaicmanager_component', $data);
            return $resultRedirect->setPath('*/*/edit', ['component_id' => $this->getRequest()->getParam('component_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
