<?php

namespace Avanti\MosaicManager\Controller\Adminhtml\Component;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Avanti\MosaicManager\Controller\Adminhtml\Component;
use Avanti\MosaicManager\Model\ComponentFactory;

class Edit extends Component
{
    private $componentFactory;
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @param Context     $context
     * @param Registry    $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        DataPersistorInterface $dataPersistor,
        ComponentFactory $componentFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->dataPersistor = $dataPersistor;
        $this->componentFactory = $componentFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action.
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $componentId = $this->getRequest()->getParam('component_id');
        $mosaicId = $this->getRequest()->getParam('mosaic_id');
        if (isset($mosaicId)) {
            $this->dataPersistor->set('avanti_mosaicmanager_mosaic_id', $this->getRequest()->getParam('mosaic_id'));
        }
        $componentModel = $this->componentFactory->create();
        if ($componentId) {
            $componentModel->load($componentId);
            if (!$componentModel->getId()) {
                $this->messageManager->addErrorMessage(__('This Component no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('avanti_mosaicmanager_component', $componentModel);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $componentId ? __('Edit Component') : __('New Component'),
            $componentId ? __('Edit Component') : __('New Component')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Components'));
        $resultPage->getConfig()->getTitle()->prepend($componentModel->getId() ? __('Edit Component %1', $componentModel->getId()) : __('New Component'));
        return $resultPage;
    }
}
