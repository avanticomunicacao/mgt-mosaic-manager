# Módulo para Magento 2 Avanti MosaicManager

    ``penseavanti/module-mosaic-manager``

## Principais funcionalidades

Este módulo tem a função de gerenciar mosaicos e seus componentes, como um gerenciados de conteúdos estáticos, porém de uma maneira facilitada.

## Instalação

### Modo 1: Arquivo ZIP

 - Descompactar o arquivo ZIP na pasta `app/code/Avanti`
 - Habilitar o módulo executando no terminal `php bin/magento module:enable Avanti_MosaicManager`
 - Aplique atualizações do banco de dados executando `php bin/magento setup:upgrade`
 - Limpe o cache `php bin/magento cache:flush`

### Modo 2: Composer

 - Inclua o repositório do Composer na configuração executando `composer config repositories.mgt-mosaicmanager git https://bitbucket.org/avanticomunicacao/mgt-mosaic-manager.git`
 - Instale o módulo via Composer `composer require penseavanti/module-mosaic-manager`
 - Habilitar o módulo executando no terminal `php bin/magento module:enable Avanti_MosaicManager`
 - Aplique atualizações do banco de dados executando `php bin/magento setup:upgrade`\*
 - Limpe o cache `php bin/magento cache:flush`

## Cadastros e Configuração

### Cadastro de Mosaicos e Componentes

Acessando via painel administrativo o caminho Conteúdo->Avanti, você pode realizar o cadastro de mosaicos e componentes.

### Vinculando os componentes com os Mosaicos

Dentro da edição do mosaico, existe uma sessão chamada "Componentes deste Mosaico".
Nesta área existe uma tabela com componentes que fazem parte do Mosaico que você está editando.
Se você for na opção "Novo componente", ao cadastrar um componente, automaticamente ele será vinculado ao Mosaico.

### Configuração

Para que as informações que foram cadastradas no painel administrativo apareçam no frontend de sua loja, deve ser feito o seguinte:

Primeiramente você deve chamar o bloco principal do módulo, que pega as informações e renderiza no frontend e passar por argumento
o CÓDIGO do seu Mosaico. É desta forma que será filtrado do mosaico:
O parâmetro sempre deve ser ```code```.

Exemplo:
```xml
<block class="Avanti\MosaicManager\Block\Index" name="main" template="Vendor_Module::homepage/page.phtml">
    <arguments>
        <argument name="code" xsi:type="string">mosaico_teste</argument>
    </arguments>
</block>
```

No arquivo phtml, basta chamar o método getComponentsByMosaicCode:

```
/** @var \Avanti\MosaicManager\Block\Index $block */
$components = $block->getComponentsByMosaicCode();
```

Existe um arquivo que serve como um exemplo:
```view/frontend/templates/mosaicmanager.phtml```
