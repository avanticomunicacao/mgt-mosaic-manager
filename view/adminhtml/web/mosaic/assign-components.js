define([
    'mage/adminhtml/grid'
], function () {
    'use strict';

    return function (config) {
        var selectedComponents = config.selectedComponents,
            mosaicComponents = $H(selectedComponents),
            gridJsObject = window[config.gridJsObjectName],
            tabIndex = 1000;

        $('in_mosaic_components').value = Object.toJSON(mosaicComponents);

        /**
         * Register Mosaic Component
         *
         * @param {Object} grid
         * @param {Object} element
         * @param {Boolean} checked
         */
        function registerMosaicComponent(grid, element, checked) {
            if (checked) {
                if (element.positionElement) {
                    element.positionElement.disabled = false;
                    mosaicComponents.set(element.value, element.positionElement.value);
                }
            } else {
                if (element.positionElement) {
                    element.positionElement.disabled = true;
                }
                mosaicComponents.unset(element.value);
            }
            $('in_mosaic_components').value = Object.toJSON(mosaicComponents);
            grid.reloadParams = {
                'selected_components[]': mosaicComponents.keys()
            };
        }

        /**
         * Click on component row
         *
         * @param {Object} grid
         * @param {String} event
         */
        function mosaicComponentRowClick(grid, event) {
            var trElement = Event.findElement(event, 'tr'),
                isInput = Event.element(event).tagName === 'INPUT',
                checked = false,
                checkbox = null;

            if (trElement) {
                checkbox = Element.getElementsBySelector(trElement, 'input');

                if (checkbox[0]) {
                    checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                    gridJsObject.setCheckboxChecked(checkbox[0], checked);
                }
            }
        }

        /**
         * Change component position
         *
         * @param {String} event
         */
        function positionChange(event) {
            var element = Event.element(event);

            if (element && element.checkboxElement && element.checkboxElement.checked) {
                mosaicComponents.set(element.checkboxElement.value, element.value);
                $('in_mosaic_components').value = Object.toJSON(mosaicComponents);
            }
        }

        /**
         * Initialize mosaic component row
         *
         * @param {Object} grid
         * @param {String} row
         */
        function mosaicComponentRowInit(grid, row) {
            var checkbox = $(row).getElementsByClassName('checkbox')[0],
                position = $(row).getElementsByClassName('input-text')[0];

            if (checkbox && position) {
                checkbox.positionElement = position;
                position.checkboxElement = checkbox;
                position.disabled = !checkbox.checked;
                position.tabIndex = tabIndex++;
                Event.observe(position, 'keyup', positionChange);
            }
        }

        gridJsObject.rowClickCallback = mosaicComponentRowClick;
        gridJsObject.initRowCallback = mosaicComponentRowInit;
        gridJsObject.checkboxCheckCallback = registerMosaicComponent;

        if (gridJsObject.rows) {
            gridJsObject.rows.each(function (row) {
                mosaicComponentRowInit(gridJsObject, row);
            });
        }
    };
});
