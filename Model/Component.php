<?php

namespace Avanti\MosaicManager\Model;

use Magento\Framework\Registry;
use Magento\Framework\Model\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Catalog\Model\ImageUploader;
use Avanti\MosaicManager\Api\Data\ComponentInterfaceFactory;
use Avanti\MosaicManager\Model\ResourceModel\Component as ResourceComponent;
use Avanti\MosaicManager\Model\ResourceModel\Component\Collection as CollectionComponent;

class Component extends AbstractModel
{
    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var ComponentInterfaceFactory
     */
    private $componentDataFactory;

    /**
     * @var ImageUploader
     */
    private $imageUploader;

    protected $_eventPrefix = 'avanti_mosaicmanager_component';

    /**
     * @param Context                $context
     * @param Registry               $registry
     * @param ComponentInterfaceFactory $componentDataFactory
     * @param DataObjectHelper       $dataObjectHelper
     * @param ResourceComponent         $resource
     * @param CollectionComponent       $resourceCollection
     * @param array                  $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ComponentInterfaceFactory $componentDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceComponent $resource,
        CollectionComponent $resourceCollection,
        ImageUploader $imageUploader,
        array $data = []
    ) {
        $this->componentDataFactory = $componentDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->imageUploader = $imageUploader;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve component model with component data.
     *
     * @return ComponentInterface
     */
    public function getDataModel()
    {
        $componentData = $this->getData();

        $componentDataObject = $this->componentDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $componentDataObject,
            $componentData,
            ComponentInterface::class
        );

        return $componentDataObject;
    }

    /**
     * Processing object before save data.
     *
     * @return $this
     */
    public function beforeSave()
    {
        foreach (['image'] as $field) {
            $this->processImages($field);
        }

        return parent::beforeSave();
    }

    /**
     * Set uploaded images to save data.
     *
     * @param array  $data
     * @param string $fieldName
     *
     * @return array
     */
    protected function processImages($fieldName)
    {
        $data = $this->getData($fieldName);

        if (isset($data[0]['name']) && isset($data[0]['tmp_name'])) {
            $this->imageUploader->moveFileFromTmp($data[0]['name']);
            $this->setData($fieldName, $data[0]['name']);
        } elseif (isset($data[0]['name']) && !isset($data[0]['tmp_name'])) {
            $this->setData($fieldName, $data[0]['name']);
        } else {
            $this->setData($fieldName, null);
        }
    }
}
