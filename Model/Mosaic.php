<?php

namespace Avanti\MosaicManager\Model;

use Magento\Framework\Registry;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Api\DataObjectHelper;
use Avanti\MosaicManager\Api\Data\MosaicInterface;
use Avanti\MosaicManager\Api\Data\MosaicInterfaceFactory;
use Avanti\MosaicManager\Model\ResourceModel\Mosaic as ResourceMosaic;
use Avanti\MosaicManager\Model\ResourceModel\Mosaic\Collection as CollectionMosaic;

class Mosaic extends AbstractModel
{
    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var MosaicInterfaceFactory
     */
    protected $mosaicDataFactory;

    protected $_eventPrefix = 'avanti_mosaicmanager_mosaic';

    /**
     * @param Context                $context
     * @param Registry               $registry
     * @param MosaicInterfaceFactory $mosaicDataFactory
     * @param DataObjectHelper       $dataObjectHelper
     * @param ResourceMosaic         $resource
     * @param CollectionMosaic       $resourceCollection
     * @param array                  $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        MosaicInterfaceFactory $mosaicDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceMosaic $resource,
        CollectionMosaic $resourceCollection,
        array $data = []
    ) {
        $this->mosaicDataFactory = $mosaicDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve mosaic model with mosaic data.
     *
     * @return MosaicInterface
     */
    public function getDataModel()
    {
        $mosaicData = $this->getData();

        $mosaicDataObject = $this->mosaicDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $mosaicDataObject,
            $mosaicData,
            MosaicInterface::class
        );

        return $mosaicDataObject;
    }

    /**
     * Retrieve array of components id's for mosaic.
     *
     * The array returned has the following format:
     * array($componentId => $position)
     *
     * @return array
     */
    public function getComponentsPosition()
    {
        if (!$this->getId()) {
            return [];
        }

        $array = $this->getData('components_position');
        if ($array === null) {
            $array = $this->getResource()->getComponentsPosition($this);
            $this->setData('components_position', $array);
        }

        return $array;
    }
}
