<?php

namespace Avanti\MosaicManager\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class MosaicComponent extends AbstractDb
{
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('avanti_mosaicmanager_mosaiccomponent', 'mosaic_component_id');
    }
}
