<?php

namespace Avanti\MosaicManager\Model\ResourceModel\Mosaic;

use Avanti\MosaicManager\Model\Mosaic as ModelMosaic;
use Avanti\MosaicManager\Model\ResourceModel\Mosaic as ResourceMosaic;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init(ModelMosaic::class, ResourceMosaic::class);
    }
}
