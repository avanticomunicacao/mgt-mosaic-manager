<?php

namespace Avanti\MosaicManager\Model\ResourceModel;

use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Avanti\MosaicManager\Model\Mosaic as MosaicModel;

class Mosaic extends AbstractDb
{
    /**
     * Mosaic components table name.
     *
     * @var string
     */
    protected $mosaicComponentsTable;

    /**
     * Core event manager proxy.
     *
     * @var ManagerInterface
     */
    protected $eventManager = null;

    /**
     * @param Context          $context
     * @param ManagerInterface $eventManager
     * @param string           $connectionName
     */
    public function __construct(
        Context $context,
        ManagerInterface $eventManager,
        $connectionName = null
    ) {
        $this->eventManager = $eventManager;
        parent::__construct($context, $connectionName);
    }

    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('avanti_mosaicmanager_mosaic', 'mosaic_id');
    }

    /**
     * Mosaic component table name getter.
     *
     * @return string
     */
    public function getMosaicComponentsTable()
    {
        if (!$this->mosaicComponentsTable) {
            $this->mosaicComponentsTable = $this->getTable('avanti_mosaicmanager_mosaic_component');
        }

        return $this->mosaicComponentsTable;
    }

    /**
     * Get positions of associated to mosaic components.
     *
     * @param MosaicModel $mosaic
     *
     * @return array
     */
    public function getComponentsPosition(MosaicModel $mosaic)
    {
        $select = $this->getConnection()->select()->from(
            $this->getMosaicComponentsTable(),
            ['component_id', 'position']
        )->where(
            'mosaic_id = :mosaic_id'
        );
        $bind = ['mosaic_id' => (int) $mosaic->getMosaicId()];

        return $this->getConnection()->fetchPairs($select, $bind);
    }

    /**
     * Process mosaic data after save mosaic object
     * save related component ids and update path value.
     *
     * @param AbstractModel $object
     *
     * @return $this
     */
    protected function _afterSave(AbstractModel $object)
    {
        $this->_saveMosaicComponents($object);
        return parent::_afterSave($object);
    }

    /**
     * Save mosaic components relation.
     *
     * @param MosaicModel $mosaic
     *
     * @return $this
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _saveMosaicComponents(MosaicModel $mosaic)
    {
        $mosaic->setIsChangedComponentList(false);
        $mosaicId = $mosaic->getMosaicId();

        $components = $mosaic->getPostedComponents();
        $oldComponents = $mosaic->getComponentsPosition();

        if (!isset($mosaic['is_new_mosaic']) && $components === null) {
            $components = $oldComponents;
            $components[$mosaic['component_id']] = '0';
        }

        $insert = array_diff_key($components, $oldComponents);
        $delete = array_diff_key($oldComponents, $components);

        /*
         * Find component ids which are presented in both arrays
         * and saved before (check $oldComponents array)
         */
        $update = array_intersect_key($components, $oldComponents);
        $update = array_diff_assoc($update, $oldComponents);

        $connection = $this->getConnection();

        /*
         * Delete components from mosaic
         */
        if (!empty($delete)) {
            $cond = ['component_id IN(?)' => array_keys($delete), 'mosaic_id = ?' => $mosaicId];
            $connection->delete($this->getMosaicComponentsTable(), $cond);
        }

        /*
         * Add component to mosaic
         */
        if (!empty($insert)) {
            $data = [];
            foreach ($insert as $componentId => $position) {
                $data[] = [
                    'mosaic_id' => (int) $mosaicId,
                    'component_id' => (int) $componentId,
                    'position' => (int) $position,
                ];
            }

            $connection->insertMultiple($this->getMosaicComponentsTable(), $data);
        }

        /*
         * Update component positions in mosaic
         */
        if (!empty($update)) {
            $newPositions = [];
            foreach ($update as $componentId => $position) {
                $delta = $position - $oldComponents[$componentId];
                if (!isset($newPositions[$delta])) {
                    $newPositions[$delta] = [];
                }
                $newPositions[$delta][] = $componentId;
            }

            foreach ($newPositions as $delta => $componentIds) {
                $bind = ['position' => new \Zend_Db_Expr("position + ({$delta})")];
                $where = ['mosaic_id = ?' => (int) $mosaicId, 'component_id IN (?)' => $componentIds];
                $connection->update($this->getMosaicComponentsTable(), $bind, $where);
            }
        }

        if (!empty($insert) || !empty($delete)) {
            $componentIds = array_unique(array_merge(array_keys($insert), array_keys($delete)));
            $this->eventManager->dispatch(
                'avanti_mosaicmanager_mosaic_component_change_components',
                ['mosaic' => $mosaic, 'component_ids' => $componentIds]
            );

            $mosaic->setChangedComponentIds($componentIds);
        }

        if (!empty($insert) || !empty($update) || !empty($delete)) {
            $mosaic->setIsChangedComponentList(true);
            $componentIds = array_keys($insert + $delete + $update);
            $mosaic->setAffectedComponentIds($componentIds);
        }

        return $this;
    }
}
