<?php

namespace Avanti\MosaicManager\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Component extends AbstractDb
{
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('avanti_mosaicmanager_component', 'component_id');
    }
}
