<?php

namespace Avanti\MosaicManager\Model\ResourceModel\Component;

use Avanti\MosaicManager\Model\Component as ModelComponent;
use Avanti\MosaicManager\Model\ResourceModel\Component as ResourceComponent;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init(ModelComponent::class, ResourceComponent::class);
    }
}
