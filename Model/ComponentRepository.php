<?php

namespace Avanti\MosaicManager\Model;

use Exception;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Avanti\MosaicManager\Api\ComponentRepositoryInterface;
use Avanti\MosaicManager\Api\Data\ComponentSearchResultsInterfaceFactory;
use Avanti\MosaicManager\Api\Data\ComponentInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Avanti\MosaicManager\Model\ResourceModel\Component as ResourceComponent;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Exception\NoSuchEntityException;
use Avanti\MosaicManager\Model\ResourceModel\Component\CollectionFactory as ComponentCollectionFactory;
use Avanti\MosaicManager\Api\Data\ComponentInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

class ComponentRepository implements ComponentRepositoryInterface
{
    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var componentFactory
     */
    private $componentFactory;

    /**
     * @var ComponentSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var DataObjectProcessor
     */
    private $dataObjectProcessor;

    /**
     * @var JoinProcessorInterface
     */
    private $extensionAttributesJoinProcessor;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var ExtensibleDataObjectConverter
     */
    private $extensibleDataObjectConverter;

    /**
     * @var ResourceComponent
     */
    private $resource;

    /**
     * @var ComponentInterfaceFactory
     */
    private $datacomponentFactory;

    /**
     * @var ComponentCollectionFactory
     */
    private $componentCollectionFactory;

    /**
     * @param ResourceComponent                      $resource
     * @param componentFactory                       $componentFactory
     * @param ComponentInterfaceFactory              $datacomponentFactory
     * @param ComponentCollectionFactory             $componentCollectionFactory
     * @param ComponentSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper                    $dataObjectHelper
     * @param DataObjectProcessor                 $dataObjectProcessor
     * @param StoreManagerInterface               $storeManager
     * @param CollectionProcessorInterface        $collectionProcessor
     * @param JoinProcessorInterface              $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter       $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceComponent $resource,
        componentFactory $componentFactory,
        ComponentInterfaceFactory $datacomponentFactory,
        ComponentCollectionFactory $componentCollectionFactory,
        ComponentSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->componentFactory = $componentFactory;
        $this->componentCollectionFactory = $componentCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->datacomponentFactory = $datacomponentFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(ComponentInterface $component)
    {
        $componentData = $this->extensibleDataObjectConverter->toNestedArray($component, [], ComponentInterface::class);
        $componentModel = $this->componentFactory->create()->setData($componentData);

        try {
            $this->resource->save($componentModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the component: %1',
                $exception->getMessage()
            ));
        }

        return $componentModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($componentId)
    {
        $component = $this->componentFactory->create();
        $this->resource->load($component, $componentId);
        if (!$component->getId()) {
            throw new NoSuchEntityException(__('Component with id "%1" does not exist.', $componentId));
        }

        return $component->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $collection = $this->componentCollectionFactory->create();
        $this->extensionAttributesJoinProcessor->process($collection, ComponentInterface::class);
        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(ComponentInterface $component)
    {
        try {
            $componentModel = $this->componentFactory->create();
            $this->resource->load($componentModel, $component->getComponentId());
            $this->resource->delete($componentModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Component: %1',
                $exception->getMessage()
            ));
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($componentId)
    {
        return $this->delete($this->get($componentId));
    }
}
