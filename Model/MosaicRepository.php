<?php

namespace Avanti\MosaicManager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Avanti\MosaicManager\Api\Data\MosaicSearchResultsInterfaceFactory;
use Avanti\MosaicManager\Model\ResourceModel\Mosaic\CollectionFactory as MosaicCollectionFactory;
use Avanti\MosaicManager\Model\ResourceModel\Mosaic as ResourceMosaic;
use Avanti\MosaicManager\Api\Data\MosaicInterfaceFactory;
use Avanti\MosaicManager\Api\MosaicRepositoryInterface;
use Avanti\MosaicManager\Api\Data\MosaicInterface;

class MosaicRepository implements MosaicRepositoryInterface
{
    private $collectionProcessor;
    private $storeManager;

    protected $dataMosaicFactory;
    protected $dataObjectHelper;
    protected $dataObjectProcessor;
    protected $extensibleDataObjectConverter;
    protected $extensionAttributesJoinProcessor;
    protected $mosaicCollectionFactory;
    protected $mosaicFactory;
    protected $resource;
    protected $searchResultsFactory;

    /**
     * @param ResourceMosaic $resource
     * @param MosaicFactory $mosaicFactory
     * @param MosaicInterfaceFactory $dataMosaicFactory
     * @param MosaicCollectionFactory $mosaicCollectionFactory
     * @param MosaicSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceMosaic $resource,
        MosaicFactory $mosaicFactory,
        MosaicInterfaceFactory $dataMosaicFactory,
        MosaicCollectionFactory $mosaicCollectionFactory,
        MosaicSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->mosaicFactory = $mosaicFactory;
        $this->mosaicCollectionFactory = $mosaicCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataMosaicFactory = $dataMosaicFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(MosaicInterface $mosaic)
    {
        $mosaicData = $this->extensibleDataObjectConverter->toNestedArray(
            $mosaic,
            [],
            MosaicInterface::class
        );

        $mosaicModel = $this->mosaicFactory->create()->setData($mosaicData);

        try {
            $this->resource->save($mosaicModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the mosaic: %1',
                $exception->getMessage()
            ));
        }

        return $mosaicModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($mosaicId)
    {
        $mosaic = $this->mosaicFactory->create();
        $this->resource->load($mosaic, $mosaicId);
        if (!$mosaic->getId()) {
            throw new NoSuchEntityException(__('Mosaic with id "%1" does not exist.', $mosaicId));
        }

        return $mosaic->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $collection = $this->mosaicCollectionFactory->create();
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            MosaicInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(MosaicInterface $mosaic)
    {
        try {
            $mosaicModel = $this->mosaicFactory->create();
            $this->resource->load($mosaicModel, $mosaic->getMosaicId());
            $this->resource->delete($mosaicModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Mosaic: %1',
                $exception->getMessage()
            ));
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($mosaicId)
    {
        return $this->delete($this->get($mosaicId));
    }
}
