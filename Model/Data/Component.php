<?php

namespace Avanti\MosaicManager\Model\Data;

use Avanti\MosaicManager\Api\Data\ComponentInterface;
use Magento\Framework\Api\AbstractExtensibleObject;

/**
 * Class Component
 * @package Avanti\MosaicManager\Model\Data
 */
class Component extends AbstractExtensibleObject implements ComponentInterface
{
    /**
     * Get component_id.
     *
     * @return string|null
     */
    public function getComponentId()
    {
        return $this->_get(self::COMPONENT_ID);
    }

    /**
     * Set component_id.
     *
     * @param string $componentId
     *
     * @return ComponentInterface
     */
    public function setComponentId($componentId)
    {
        return $this->setData(self::COMPONENT_ID, $componentId);
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ComponentInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get image.
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->_get(self::IMAGE);
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return ComponentInterface
     */
    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->_get(self::URL);
    }

    /**
     * Set url.
     *
     * @param string $url
     *
     * @return ComponentInterface
     */
    public function setUrl($url)
    {
        return $this->setData(self::URL, $url);
    }

    /**
     * Get created_at.
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at.
     *
     * @param string $createdAt
     *
     * @return ComponentInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at.
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at.
     *
     * @param string $updatedAt
     *
     * @return ComponentInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get alt.
     *
     * @return string|null
     */
    public function getAlt()
    {
        return $this->_get(self::ALT);
    }

    /**
     * Set alt.
     *
     * @param string $alt
     *
     * @return ComponentInterface
     */
    public function setAlt($alt)
    {
        return $this->setData(self::ALT, $alt);
    }

    /**
     * Get text one.
     *
     * @return string|null
     */
    public function getTextOne()
    {
        return $this->_get(self::TEXT_ONE);
    }

    /**
     * Set text one.
     *
     * @param string $textOne
     *
     * @return ComponentInterface
     */
    public function setTextOne($textOne)
    {
        return $this->setData(self::TEXT_ONE, $textOne);
    }

    /**
     * Get text two.
     *
     * @return string|null
     */
    public function getTextTwo()
    {
        return $this->_get(self::TEXT_TWO);
    }

    /**
     * Set text two.
     *
     * @param string $textTwo
     *
     * @return ComponentInterface
     */
    public function setTextTwo($textTwo)
    {
        return $this->setData(self::TEXT_TWO, $textTwo);
    }

    /**
     * Get text three.
     *
     * @return string|null
     */
    public function getTextThree()
    {
        return $this->_get(self::TEXT_THREE);
    }

    /**
     * Set text three.
     *
     * @param string $textThree
     *
     * @return ComponentInterface
     */
    public function setTextThree($textThree)
    {
        return $this->setData(self::TEXT_THREE, $textThree);
    }

    /**
     * Get text four.
     *
     * @return string|null
     */
    public function getTextFour()
    {
        return $this->_get(self::TEXT_FOUR);
    }

    /**
     * Set text four.
     *
     * @param string $textFour
     *
     * @return ComponentInterface
     */
    public function setTextFour($textFour)
    {
        return $this->setData(self::TEXT_FOUR, $textFour);
    }

    /**
     * Get text five.
     *
     * @return string|null
     */
    public function getTextFive()
    {
        return $this->_get(self::TEXT_FIVE);
    }

    /**
     * Set text five.
     *
     * @param string $textFive
     *
     * @return ComponentInterface
     */
    public function setTextFive($textFive)
    {
        return $this->setData(self::TEXT_FIVE, $textFive);
    }

    /**
     * Get text six.
     *
     * @return string|null
     */
    public function getTextSix()
    {
        return $this->_get(self::TEXT_SIX);
    }

    /**
     * Set text six.
     *
     * @param string $textSix
     *
     * @return ComponentInterface
     */
    public function setTextSix($textSix)
    {
        return $this->setData(self::TEXT_SIX, $textSix);
    }

    /**
     * Get text seven.
     *
     * @return string|null
     */
    public function getTextSeven()
    {
        return $this->_get(self::TEXT_SEVEN);
    }

    /**
     * Set text seven.
     *
     * @param string $textSeven
     *
     * @return ComponentInterface
     */
    public function setTextSeven($textSeven)
    {
        return $this->setData(self::TEXT_SEVEN, $textSeven);
    }

    /**
     * Get text eight.
     *
     * @return string|null
     */
    public function getTextEight()
    {
        return $this->_get(self::TEXT_EIGHT);
    }

    /**
     * Set text eight.
     *
     * @param string $textEight
     *
     * @return ComponentInterface
     */
    public function setTextEight($textEight)
    {
        return $this->setData(self::TEXT_EIGHT, $textEight);
    }

    /**
     * Get text nine.
     *
     * @return string|null
     */
    public function getTextNine()
    {
        return $this->_get(self::TEXT_NINE);
    }

    /**
     * Set text nine.
     *
     * @param string $textNine
     *
     * @return ComponentInterface
     */
    public function setTextNine($textNine)
    {
        return $this->setData(self::TEXT_NINE, $textNine);
    }

    /**
     * Get text ten.
     *
     * @return string|null
     */
    public function getTextTen()
    {
        return $this->_get(self::TEXT_TEN);
    }

    /**
     * Set text ten.
     *
     * @param string $textTen
     *
     * @return ComponentInterface
     */
    public function setTextTen($textTen)
    {
        return $this->setData(self::TEXT_TEN, $textTen);
    }
}
