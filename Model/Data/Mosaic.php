<?php

namespace Avanti\MosaicManager\Model\Data;

use Avanti\MosaicManager\Api\Data\MosaicInterface;
use Magento\Framework\Api\AbstractExtensibleObject;

class Mosaic extends AbstractExtensibleObject implements MosaicInterface
{
    /**
     * Get mosaic_id.
     *
     * @return string|null
     */
    public function getMosaicId()
    {
        return $this->_get(self::MOSAIC_ID);
    }

    /**
     * Set mosaic_id.
     *
     * @param string $mosaicId
     *
     * @return MosaicInterface
     */
    public function setMosaicId($mosaicId)
    {
        return $this->setData(self::MOSAIC_ID, $mosaicId);
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return MosaicInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return MosaicInterface
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * Get active.
     *
     * @return string|null
     */
    public function getActive()
    {
        return $this->_get(self::ACTIVE);
    }

    /**
     * Set active.
     *
     * @param string $active
     *
     * @return MosaicInterface
     */
    public function setActive($active)
    {
        return $this->setData(self::ACTIVE, $active);
    }

    /**
     * Get created_at.
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at.
     *
     * @param string $createdAt
     *
     * @return \Avanti\MosaicManager\Api\Data\MosaicInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at.
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at.
     *
     * @param string $updatedAt
     *
     * @return \Avanti\MosaicManager\Api\Data\MosaicInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}
