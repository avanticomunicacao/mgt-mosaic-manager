<?php

namespace Avanti\MosaicManager\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Avanti\MosaicManager\Model\ComponentFactory;

/**
 * Class Data
 * @package Avanti\MosaicManager\Helper
 */
class Data extends AbstractHelper
{
    const CONFIG_ENABLED = 'avanti_mosaicmanager/general/enabled';
    /**
     * @var ComponentFactory
     */
    public $componentFactory;

    /**
     * Product labels media directory path.
     *
     * @var string
     */
    protected $mediaDir = '/avanti/mosaicmanager';

    /**
     * Currently selected store ID if applicable.
     *
     * @var int
     */
    protected $_storeId = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param Context               $context
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Store $store,
        ComponentFactory $componentFactory
    ){
        $this->storeManager = $storeManager;
        $this->store = $store;
        $this->componentFactory = $componentFactory;

        parent::__construct($context);
    }

    /**
     * Get config value by path.
     *
     * @param string $path
     *
     * @return mixed
     */
    public function getConfigValue($path, $storeId = null)
    {
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * Get config flag by path.
     *
     * @param string $path
     *
     * @return bool
     */
    public function getConfigFlag($path)
    {
        return $this->scopeConfig->isSetFlag($path, ScopeInterface::SCOPE_STORE, $this->_storeId);
    }

    /**
     * Return true if active and false otherwise.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->getConfigFlag(self::CONFIG_ENABLED);
    }

    /**
     * Retrieves absolute image url.
     *
     * @param $path
     *
     * @return string
     */
    public function getImageUrl($path)
    {
        if (false === strpos($path, $this->mediaDir)) {
            $path = $this->mediaDir . '/' . $path;
        }

        return $this->store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . $path;
    }

    /**
     * @param null $id
     *
     * @return Collection
     */
    public function getComponentCollection($mosaicId = null)
    {
        $collection = $this->componentFactory->create()->getCollection();
        $collection->join(
            ['mosaic_component' => $collection->getTable('avanti_mosaicmanager_mosaic_component')],
            'main_table.component_id=mosaic_component.component_id AND mosaic_component.mosaic_id=' . $mosaicId,
            ['position']
        );
        $collection->addOrder('position', 'ASC');
        return $collection;
    }
}
